namespace Taurus.Templates.CapitalCall.CapitalCallDetails
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for CapitalCallDetailsReport.
    /// </summary>
    public partial class CapitalCallDetailsReport : Telerik.Reporting.Report
    {
        public CapitalCallDetailsReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}