﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAURUS.COM.Report.Models;
using Telerik.Templates.DAL.Repository;

namespace Taurus.Templates.CapitalCall.CapitalCallStatement
{
    [DataObject]
    public class CapitalCallStatementData
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<TAURUS.COM.Report.Models.InvestorCapitalCallStatement> GetCapitalCallStatement(int? productID,
            int? fundID,
            int? classID,
            int? batchJobSummaryID,
            int? userID,
            string accountNumber,
            DateTime? effectiveDateTo,
            DateTime? effectiveDateFrom)
        {
            var param = new GetInvestorCapitalCallStatementParameter()
            {
                productID = productID,
                effectiveDateTo = effectiveDateTo,
                effectiveDateFrom = effectiveDateFrom,
                classID = classID,
                schemeID = fundID,
                userID = userID,
                accountNumber = accountNumber,
                batchJobSummaryID = batchJobSummaryID
            };
            ReportRepository invStatement = new ReportRepository();
            var list = invStatement.GetCapitalCallStatement(param).Result;
            return list;
        }
    }
}
