namespace Taurus.Templates.CapitalCall.CapitalCallStatement
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for CapitalCallStatement.
    /// </summary>
    public partial class CapitalCallStatementReport : Telerik.Reporting.Report
    {
        public CapitalCallStatementReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}