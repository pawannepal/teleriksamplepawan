﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Templates.DAL.Repository;
using TAURUS.COM.Report.Models;


namespace Taurus.Templates.CapitalCall.CapitalCallNotice
{
    [DataObject]
    public class CapitalCallNoticeData
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<TAURUS.COM.Report.Models.CapitalCallNotice> GetCapitalCallNotice(int? productID,
            int? fundID,
            int? classID,
            int? batchJobSummaryID,
            int? userID,
            string accountNumber,
            DateTime? effectiveDateTo,
            DateTime? effectiveDateFrom)
        {
            var param = new CapitalCallNoticeParameter()
            {
                productID = productID,
                effectiveDateTo = effectiveDateTo,
                effectiveDateFrom = effectiveDateFrom,
                classID = classID,
                schemeID = fundID,
                userID = userID,
                accountNumber = accountNumber,
                batchJobSummaryID= batchJobSummaryID
            };
            ReportRepository invStatement = new ReportRepository();
            var list = invStatement.GetCapitalCallNotice(param).Result;
            return list;
        }
    }
}
