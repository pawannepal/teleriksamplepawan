﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAURUS.COM.Report.Models;
using Telerik.Templates.DAL.Repository;

namespace Taurus.Templates.InvestorStatements.TransactionConfirmationStatement
{
   
        [DataObject]
        public class TransactionConfirmationStatement
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public List<InvestorTransactionConfirmation> GetTransactionConfirmationStatement(int? productID,
               int? fundID,
               int? classID,
               int? batchJobSummaryID,
               int? userID,
               string accountNumber,
               DateTime? effectiveDateTo,
               DateTime? effectiveDateFrom)
            {
                var param = new InvestorTransactionConfirmationParameter()
                {
                    productID = productID,
                    effectiveDateTo = effectiveDateTo,
                    effectiveDateFrom = effectiveDateFrom,
                    classID = classID,
                    fundID = fundID,
                    userID = userID,
                    accountNumber = accountNumber
                };
                ReportRepository invStatement = new ReportRepository();
                var list = invStatement.GetTransactionConfirmationStatement(param).Result;
                return list;
            }
        }
    
}
