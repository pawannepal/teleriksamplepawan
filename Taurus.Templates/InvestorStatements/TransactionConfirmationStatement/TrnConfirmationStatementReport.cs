namespace Taurus.Templates.InvestorStatements.TransactionConfirmationStatement
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for TrnConfirmationStatementReport.
    /// </summary>
    public partial class TrnConfirmationStatementReport : Telerik.Reporting.Report
    {
        public TrnConfirmationStatementReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}