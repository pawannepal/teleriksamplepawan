﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAURUS.COM.Report.Models;
using Telerik.Templates.DAL.Repository;

namespace Taurus.Templates.Statements
{
   [DataObject]
    public class TransactionStatement : List<InvestorTransactionStatement>
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<InvestorTransactionStatement> GetStatements(
            int? productID, 
            int? fundID, 
            int? classID, 
            int? userID,
            string accountNumber,
            DateTime? effectiveDateTo,
            DateTime? effectiveDateFrom)
        {
            List<InvestorTransactionStatement> list = new List<InvestorTransactionStatement>();
            ReportRepository invStatement = new ReportRepository();
            list = invStatement.InvestorTransactionStatement(new InvestorTransactionStatementParameter()
                {
                    productID = productID,
                    effectiveDateTo = effectiveDateTo,
                    effectiveDateFrom=effectiveDateFrom,
                    classID=classID,
                    fundID=fundID,
                    userID=userID,
                    accountNumber=accountNumber
                }
            );

            return list;
        }
    }
}
