﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAURUS.COM.Report.Models;
using Telerik.Templates.DAL.Repository;

namespace Taurus.Templates.Commission.CommissionDetails
{
    [DataObject]
    public class CommissionDetailsData
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<MGMTCommissionDetails> GetCommissionDetails(int? productID,
          int? fundID,
          int? classID,
          int? batchJobSummaryID,
          int? userID,
          int? accountNumber,
          DateTime? effectiveDateTo,
          DateTime? effectiveDateFrom)
        {
            var param = new MGMTCommissionDetailsParameter()
            {
                productID = productID,
                effectiveDateTo = effectiveDateTo,
                effectiveDateFrom = effectiveDateFrom,
                classID = classID,
                schemeID = fundID,
                userID = userID,
                accountNumber = accountNumber,
                batchJobSummaryID = batchJobSummaryID
            };
            ReportRepository invStatement = new ReportRepository();
            var list = invStatement.GetCommissionDetails(param).Result;
            return list;
        }
    }
}
