﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAURUS.COM.Report.Models;
using Telerik.Templates.DAL.Repository;

namespace Taurus.Templates.Distribution.DistributionPaymentSummary
{
    [DataObject]
    public class DistributionPaymentSummaryData
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<TAURUS.COM.Report.Models.MGMTDistributionPaymentSummary> GetDistributionPaymentSummary(int? productID,
           int? fundID,
           int? classID,
           int? batchJobSummaryID,
           int? userID,
           string accountNumber,
           DateTime? effectiveDateTo,
           DateTime? effectiveDateFrom)
        {
            var param = new MGMTDistributionPaymentSummaryParameter()
            {
                productID = productID,
                effectiveDateTo = effectiveDateTo,
                effectiveDateFrom = effectiveDateFrom,
                classID = classID,
                fundID = fundID,
                userID = userID,
                batchJobSummaryID = batchJobSummaryID
            };
            ReportRepository invStatement = new ReportRepository();
            var list = invStatement.GetDistributionPaymentSummary(param).Result;
            return list;
        }
    }
}
