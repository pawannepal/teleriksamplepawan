namespace Taurus.Templates.Distribution.DistributionPaymentSummary
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for DistributionPaymentSummaryReport.
    /// </summary>
    public partial class DistributionPaymentSummaryReport : Telerik.Reporting.Report
    {
        public DistributionPaymentSummaryReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}