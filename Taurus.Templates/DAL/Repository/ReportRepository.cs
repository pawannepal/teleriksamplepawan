﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using TAURUS.COM.Report.Models;

namespace Telerik.Templates.DAL.Repository
{
    public class ReportRepository 
    {
        private readonly TAURUS.COM.Report.Report _reportQuery;

        public ReportRepository()
        {
            //string connStr = ConfigurationManager.ConnectionStrings["TaurusConnection"].ConnectionString;
            string connStr = "data source=13.210.103.191, 1433;initial catalog=TAURUS;user id=dev;password=test;";

            if (String.IsNullOrWhiteSpace(connStr))
            {
                throw new Exception("Configuration Error");
            }

            _reportQuery = new TAURUS.COM.Report.Report()
            {
                connectionString = connStr
            };
        }
        #region Private Functions

        public void Open() => _reportQuery?.OpenConnection();


        public void Close() => _reportQuery?.CloseConnection();
   


        public async Task<T> RunQuery<T>(Func<Task<T>> task)
        {
            try
            {
                Open();
                Task<T> result = Task.Run(task);
                await Task.WhenAll(result);
                Close();
                return (await result);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion



       public List<InvestorTransactionStatement> InvestorTransactionStatement(InvestorTransactionStatementParameter model)
        {
            try
            {
                List<InvestorTransactionStatement> result = RunQuery(() => _reportQuery.InvestorTransactionStatement(model)).Result;             
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public async Task<List<CapitalCallNotice>> GetCapitalCallNotice(CapitalCallNoticeParameter model)
        {
            try
            {
                List<CapitalCallNotice> result = await RunQuery(() => _reportQuery.CapitalCallNotice(model));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public async Task<List<InvestorCapitalCallStatement>> GetCapitalCallStatement(GetInvestorCapitalCallStatementParameter model)
        {
            try
            {
                List<InvestorCapitalCallStatement> result = await RunQuery(() => _reportQuery.InvestorCapitalCallStatement(model));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public async Task<List<MGMTCapitalCallDetails>> GetCapitalCallDetails(GetMGMTCapitalCallDetailsParameter model)
        {
            try
            {
                List<MGMTCapitalCallDetails> result = await RunQuery(() => _reportQuery.MGMTCapitalCallDetails(model));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public async Task<List<MGMTDistributionPaymentSummary>> GetDistributionPaymentSummary(MGMTDistributionPaymentSummaryParameter model)
        {
            try
            {
                List<MGMTDistributionPaymentSummary> result = await RunQuery(() => _reportQuery.MGMTDistributionPaymentSummary(model));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public async Task<List<InvestorTransactionConfirmation>> GetTransactionConfirmationStatement(InvestorTransactionConfirmationParameter model)
        {
            try
            {
                List<InvestorTransactionConfirmation> result = await RunQuery(() => _reportQuery.InvestorTransactionConfirmation(model));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public async Task<List<MGMTCommissionDetails>> GetCommissionDetails(MGMTCommissionDetailsParameter model)
        {
            try
            {
                List<MGMTCommissionDetails> result = await RunQuery(() => _reportQuery.MGMTCommissionDetails(model));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
