﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TelerikService.Service
{
    public interface IBasicAPI
    {
        Task<string> GetReportClientIDAsync();
        Task<dynamic> GetReportParametersAsync(string clientId, string reportName="");
        Task<string> GetReportInstanceIdAsync(string clientId, string reportName="");
        Task<string> CreateDocumentIdAsync(string clientId, string instanceId, string docFormat="");
        Task<Stream> DownloadAsync(string downloadUri, string fileName);
    }
}
