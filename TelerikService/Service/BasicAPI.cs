﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Taurus.Templates.Statements;
using Telerik.Reporting.Examples.CSharp;
using Telerik.Reporting.Services.AspNetCore;
using TelerikService.Models;

namespace TelerikService.Service
{
    public class BasicAPI : IBasicAPI
    {
        public HttpClient Client { get; set; }
        public BasicAPI()
        {
            Client = new HttpClient();
            Client.BaseAddress = new System.Uri("http://localhost:38752/api/reports/");
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
           
        }


        public async Task<string> GetReportClientIDAsync()
        {
            //string jsonContent = JsonConvert.SerializeObject(model);
            //HttpContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
            var response =await Client.PostAsync("clients", null);
            if (response.IsSuccessStatusCode)
            {
                var clientObject = await response.Content.ReadAsStringAsync();
                var clientID = JsonConvert.DeserializeObject<Client>(clientObject).ClientId;
                return clientID;
            }
            return null;
        }

        public async Task<dynamic> GetReportParametersAsync(string clientId, string reportName)
        {
            string fullQualifiedInvoiceReportName = typeof(StatementReport).AssemblyQualifiedName;

            if (String.IsNullOrEmpty(reportName))
                reportName = fullQualifiedInvoiceReportName;

            var reportSource = new ClientReportSource()
            {
                Report = reportName,
                ParameterValues = new Dictionary<string, object>() { }
            };
            string jsonContent = JsonConvert.SerializeObject(reportSource);
            HttpContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
            var response = await Client.PostAsync($"clients/{clientId}/parameters", content);
            if (response.IsSuccessStatusCode)
            {
                var parametersObject = await response.Content.ReadAsStringAsync();
                var parameters = JsonConvert.DeserializeObject<dynamic>(parametersObject);
                return parameters;
            }
            return null;
        }

        public async Task<string> GetReportInstanceIdAsync(string clientId, string reportName)
        {
            string fullQualifiedInvoiceReportName = typeof(StatementReport).AssemblyQualifiedName;

            if (String.IsNullOrEmpty(reportName))
                reportName = fullQualifiedInvoiceReportName;

            var reportSource = new ClientReportSource()
            {
                Report = reportName,
                ParameterValues = new Dictionary<string, object>() { }
            };
            string jsonContent = JsonConvert.SerializeObject(reportSource);
            HttpContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");

            
            var response = await Client.PostAsync($"clients/{clientId}/instances", content);
            if (response.IsSuccessStatusCode)
            {
                var instanceObject = await response.Content.ReadAsStringAsync();
                var instanceID = JsonConvert.DeserializeObject<Instance>(instanceObject).InstanceId;
                return instanceID;
            }
           
            return response.StatusCode.ToString();
        }

        public async Task<string> CreateDocumentIdAsync(string clientId, string instanceId, string docFormat)
        {
            if (String.IsNullOrEmpty(docFormat))
                docFormat = "PDF";
            var docArgs = new CreateDocumentArgs()
            {
                Format = docFormat,
                DeviceInfo = new Dictionary<string, object>()
            };
            string jsonContent = JsonConvert.SerializeObject(docArgs);
            HttpContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");

            var response = await Client.PostAsync($"clients/{clientId}/instances/{instanceId}/documents", content);
            if (response.IsSuccessStatusCode)
            {
                var documentObj = await response.Content.ReadAsStringAsync();
                var documentID = JsonConvert.DeserializeObject<Document>(documentObj).DocumentId;
                return documentID;
            }
            return response.StatusCode.ToString();
        }


        public async Task<Stream> DownloadAsync(string requestUri, string filename)
        {

            if (filename == null)
                throw new ArgumentNullException("fileName");
            try
            {
                var response = await Client.GetAsync(requestUri);
                if (response.IsSuccessStatusCode)
                {
                    var documentObj = await response.Content.ReadAsStreamAsync();
                    return documentObj;
                }
            }
            catch(Exception ex)
            {
                
            }
            return null;
        }


    }
}
