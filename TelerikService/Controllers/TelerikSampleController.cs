﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Telerik.Reporting.Cache.File;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.AspNetCore;
using System.Collections;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using TelerikService.Models;
using Newtonsoft.Json.Linq;
using TelerikService.Service;
using System;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TelerikService.Controllers
{
    [Route("api/reports")]
    public class TelerikSampleController : ReportsControllerBase
    {
        string reportsPath = string.Empty;
        private readonly IBasicAPI basicAPIs;

      
        public TelerikSampleController(IHostingEnvironment environment, IBasicAPI basicAPIs)
        {

            this.reportsPath = Path.Combine(environment.WebRootPath, "Reports");

            this.ReportServiceConfiguration = new ReportServiceConfiguration
            {
                HostAppId = "Html5DemoApp",
                Storage = new FileStorage(@"C:\Test"),
                ReportResolver = new ReportTypeResolver()
                                    .AddFallbackResolver(new ReportFileResolver(this.reportsPath)),
            };
         
            this.basicAPIs = basicAPIs;
        }

        [HttpGet("reportlist")]
        public IEnumerable<string> GetReports()
        {
            try
            {
                return Directory
               .GetFiles(this.reportsPath)
               .Select(path =>
                   Path.GetFileName(path));
            }
            catch (System.Exception)
            {
                return new List<string>() {"Welcome to CXI Report Engine"};
            }
           
        }

        [HttpPost("document")]
        public async Task<IActionResult> GetDocumentId([FromBody]ClientReportSource report)
        {

            var clientId =await basicAPIs.GetReportClientIDAsync();

            var instanceId = await basicAPIs.GetReportInstanceIdAsync(clientId, report.Report);

            var documentId = await basicAPIs.CreateDocumentIdAsync(clientId, instanceId);


            return Ok(new {clientId=clientId, instanceId=instanceId, documentId=documentId });
           
        }

    }
}
